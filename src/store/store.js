import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store(
    {
        state: {
            token: localStorage.getItem('token'),
            user: JSON.parse(localStorage.getItem('user')),
            isLoggingOut: false,
            vouchers: JSON.parse(localStorage.getItem('vouchers')) ? JSON.parse(localStorage.getItem('vouchers')) : '',
            loading: false,
            currentStore: JSON.parse(localStorage.getItem('currentStore'))
        },
        getters: {
            isLoggedIn: state => {
                return !!state.token
            },
            isLoggingOut: state => {
                return state.isLoggingOut
            },
            user: state => {
                return state.user
            },
            activeVouchers: state => {
                if ( typeof state.vouchers.length !== 'undefined' ) {
                    return state.vouchers.length
                }
                return 0
            },
            loading: state => {
                return state.loading
            },
            currentStore: state => {
                return state.currentStore
            }
        },
        mutations: {
            auth_success(state, {token, user}) {
                state.token = token
                state.user = user
                state.vouchers = user.vouchers_used_today
            },
            logout(state) {
                state.token = ''
            },
            turn_on_loading(state) {
                state.loading = true
            },
            turn_off_loading(state) {
                setTimeout( () => {
                    state.loading = false
                }, 500)
            },
            refresh_vouchers(state) {
                state.vouchers = JSON.parse(localStorage.getItem('vouchers'))
            },
            set_current_store(state, store) {
                state.currentStore = store
            },
            set_is_logging_out(state) {
                state.isLoggingOut = true
            }
        },
        actions: {
            refreshVouchers({commit}) {
                commit('refresh_vouchers')
            },
            setCurrentStore({commit}, store) {
                localStorage.setItem('currentStore', JSON.stringify(store))
                commit('set_current_store', store)
            },
            loadingOn({commit}) {
                commit('turn_on_loading')
            },
            loadingOff({commit}) {
                commit('turn_off_loading')
            },
            login({commit}, user) {
                return new Promise((resolve, reject) => {
                    axios.post('/login', user)
                        .then( (data) => {
                            const token = data.data.token
                            const user = data.data.user
                            axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
                            localStorage.setItem('user', JSON.stringify(user))
                            localStorage.setItem('vouchers', JSON.stringify(user.vouchers_used_today))
                            localStorage.setItem('token', token)
                            commit('auth_success', {token, user})
                            resolve(data)
                        })
                        .catch( (err) => {
                            localStorage.removeItem('token')
                            localStorage.removeItem('user')
                            localStorage.removeItem('vouchers')
                            reject(err)
                        })
                })
            },
            logout({commit}) {
                return new Promise((resolve, reject) => {
                    commit('set_is_logging_out')
                    localStorage.removeItem('token')
                    localStorage.removeItem('user')
                    localStorage.removeItem('vouchers')
                    delete axios.defaults.headers.common['Authorization']
                    resolve()
                })
            }
        }
    }
)