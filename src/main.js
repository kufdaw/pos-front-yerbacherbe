// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import BootstrapVue from 'bootstrap-vue'
import vmodal from 'vue-js-modal'
import lodash from 'lodash'
import axios from 'axios'
import VueEcho from 'vue-echo-laravel'
import Pusher from "pusher-js"
import VCalendar from 'v-calendar'
import VueFuse from 'vue-fuse'
import ElementUI from 'element-ui';
import locale from 'element-ui/lib/locale/lang/en'
import 'element-ui/lib/theme-chalk/index.css';
import VueApexCharts from 'vue-apexcharts'
import SyncLoader from 'vue-spinner/src/SyncLoader.vue'
import Toasted from 'vue-toasted'
import store from './store/store'

window.axios = axios;

Vue.use(BootstrapVue)
Vue.use(vmodal)
Vue.use(ElementUI, { locale });
Vue.use(lodash, lodash)
Vue.use(VCalendar)
Vue.use(VueFuse)
Vue.use(SyncLoader)
Vue.component('apexchart', VueApexCharts)
Vue.use(Toasted, {
    theme: "toasted-primary",
    position: "bottom-right",
    duration : 5000,
    type: 'success',
    className: 'toasted-success'
})

const moment = require(
    'moment')
require('moment/locale/pl')

Vue.use(require('vue-moment'), {
    moment
})

Vue.use(VueEcho, {
  broadcaster: 'pusher',
  key: '081ce0b7f38c24a4ee7c',
  cluster: 'eu',
  forceTLS: true
});

Vue.config.productionTip = false

axios.defaults.baseURL = process.env.API
const token = localStorage.getItem('token')
if (token) {
  axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
}

axios.interceptors.request.use( function (request) {
    store.dispatch('loadingOn')
    return request
}, function (error) {
    return error
});

axios.interceptors.response.use(function (response) {
    store.dispatch('loadingOff')
    return response;
}, function (error) {
    store.dispatch('loadingOff')
    Vue.toasted.show(error.response.data.error, { type: 'error', className: 'toasted-error', duration: 10000})
    Vue.toasted.show(error.response.data.message, { type: 'error', className: 'toasted-error', duration: 10000})
    Object.entries(error.response.data.errors).forEach( ([key, value]) => {
        Vue.toasted.show(key + ': ' + value[0], { type: 'error', className: 'toasted-error', duration: 10000})
    })
    return error;
});

Vue.directive('focus', {
    // When the bound element is inserted into the DOM...
    inserted: function (el) {
        // Focus the element
        el.focus()
    }
})

Vue.filter('initials', function (value) {
    return value.split(/\s/).reduce( (response, word) => response+=word.slice(0,1), '').toUpperCase()
})

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'v-calendar/lib/v-calendar.min.css';

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  components: { App },
  template: '<App/>'
})
