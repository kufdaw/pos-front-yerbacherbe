export default class UserService {

    show(id) {
        return axios.get('/users/' + id)
    }

    index() {
        return axios.get('/users')
    }

    indexSellers() {
        return axios.get('/users/sellers')
    }

    indexStore(storeId) {
        return axios.get('/shops/' + storeId + '/users')
    }

    update(user) {
        return axios.put('/users/' + user.id, user)
    }

    delete(userId) {
        return axios.delete('/users/' + userId)
    }

    store(user) {
        return axios.post('/users', user)
    }

    vouchers() {
        return axios.get('/vouchers')
    }

    updateSelf(user) {
        return axios.put('/users/update-self', user)
    }

    showMe() {
        return axios.get('/users/me')
    }
}