export default class OrderService {

    index(storeId) {
        return axios.get('/shops/' + storeId + '/orders/active')
    }

    create(storeId, order) {
        return axios.post('/shops/' + storeId + '/orders', order)
    }

    updateStatus(orderId, statusObject) {
        return axios.put('/orders/' + orderId, statusObject)
    }

    history(storeId, startDate, endDate) {
        return axios.get('/shops/' + storeId + '/orders/history?start_date=' + startDate + '&end_date=' + endDate)
    }
}