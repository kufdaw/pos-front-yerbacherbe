export default class StoreService {

    index() {
        return axios.get('/shops')
    }

    show(storeId) {
        return axios.get('/shops/' + storeId)
    }

    create(store) {
        return axios.post('/shops', store)
    }

    update(store) {
        return axios.put('/shops/' + store.id, store)
    }
}