import Vue from 'vue'
import Router from 'vue-router'
import store from '../store/store'

import Products from '@/components/Product/Products'
import Orders from '@/components/Order/Orders'
import History from '@/components/History'
// import UserList from "@/components/User/UserList"
import AccessDisallowed from "@/components/AccessDisallowed"
import Login from "@/components/Login"
import CustomerOrders from "@/components/Order/CustomerOrders"
import AdminHome from "@/components/Admin/AdminHome"
import AdminUsersList from "@/components/Admin/AdminUsersList"
import AdminNewUser from "@/components/Admin/AdminNewUser"
import AdminEditUser from "@/components/Admin/AdminEditUser"
import AdminStoresList from "@/components/Admin/AdminStoresList"
import AdminNewStore from "@/components/Admin/AdminNewStore"
import AdminEditStore from "@/components/Admin/AdminEditStore"
import ChooseStore from '@/components/ChooseStore'
import MyAccount from '@/components/MyAccount'
import Store from "@/components/Store/Store"

Vue.use(Router)

let router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/admin',
      name: 'AdminHome',
      component: AdminHome,
      meta: {
        requiresAuth: true,
        forAdmin: true
      }
    },
    {
      path: '/admin/users',
      name: 'AdminUsersList',
      component: AdminUsersList,
      meta: {
        requiresAuth: true,
        forAdmin: true
      }
    },
    {
      path: '/admin/users/new',
      name: 'AdminNewUser',
      component: AdminNewUser,
      meta: {
        requiresAuth: true,
        forAdmin: true
      }
    },
    {
      path: '/admin/users/:userId',
      name: 'AdminEditUser',
      component: AdminEditUser,
      meta: {
        requiresAuth: true,
        forAdmin: true
      }
    },
    {
      path: '/admin/stores',
      name: 'AdminStoresList',
      component: AdminStoresList,
      meta: {
        requiresAuth: true,
        forAdmin: true
      }
    },
    {
      path: '/admin/stores/new',
      name: 'AdminNewStore',
      component: AdminNewStore,
      meta: {
        requiresAuth: true,
        forAdmin: true
      }
    },
    {
      path: '/admin/stores/:adminStoreId',
      name: 'AdminEditStore',
      component: AdminEditStore,
      meta: {
        requiresAuth: true,
        forAdmin: true
      }
    },
    {
      path: '/stores/:storeId',
      name: 'Store',
      component: Store,
      meta: {
        requiresAuth: true,
        forSeller: true
      },
    },
    {
      path: '/stores/:storeId/products',
      name: 'Products',
      component: Products,
      meta: {
        requiresAuth: true,
        forSeller: true
      }
    },
    {
      path: '/stores/:storeId/orders',
      name: 'Orders',
      component: Orders,
      meta: {
        requiresAuth: true,
        forSeller: true
      }
    },
    {
      path: '/stores/:storeId/history',
      name: 'History',
      component: History,
      meta: {
        requiresAuth: true,
        forManager: true
      }
    },
    {
      path: '/choose-store',
      name: ChooseStore,
      component: ChooseStore,
      meta: {
        requiresAuth: true,
      }
    },
    {
      path: '/stores/:storeId/customer-orders',
      name: 'CustomerOrders',
      component: CustomerOrders,
      meta: {
        requiredAuth: true,
        forSeller: true
      }
    },
    {
      path: '/error',
      name: 'AccessDisallowed',
      component: AccessDisallowed
    },
    {
      path: '/stores/:storeId/my-account',
      name: 'MyAccount',
      component: MyAccount,
      meta: {
        requiredAuth: true
      }
    },
    {
      path: '/login',
      name: 'Login',
      component: Login,
      beforeEnter: (to, from, next) => {
        if (store.getters.isLoggedIn && !store.getters.isLoggingOut) {
          next('/')
          return
        }
        next()
      }
    },
    {
      path: '/',
      beforeEnter: (to, from, next) => {
        if (store.getters.isLoggedIn) {
          if (store.getters.user.role === 'admin') {
            next('/admin/stores')
            return
          } else {
            next('/choose-store')
            return
          }
        }
        next('/login')
      }
    },
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (store.getters.isLoggedIn) {
      if (to.meta.forAdmin) {
        if (store.getters.user.role === 'admin') {
          next()
          return
        } else {
          next('/error')
          return
        }
      } else if (to.meta.forManager) {
        if ( typeof (_.find(store.getters.user.shops_as_manager, { id: parseInt(to.params.storeId) } )) !== 'undefined' || store.getters.user.role === 'admin') {
          next()
          return
        } else {
          next('/error')
          return
        }
      } else if (to.meta.forSeller) {
        if (typeof (_.find(store.getters.user.shops_as_seller, { id: parseInt(to.params.storeId) } )) !== 'undefined' || typeof (_.find(store.getters.user.shops_as_manager, { id: parseInt(to.params.storeId) } )) !== 'undefined' || store.getters.user.role === 'admin') {
          next()
          return
        } else {
          next('/error')
          return
        }
      }
      next()
      return
    }
    next('/login')
  } else {
    next()
  }
})

export default router