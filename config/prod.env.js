const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge({
    NODE_ENV: '"production"',
    API: '"https://api-polcogames.earth.polcode.com"',
})

